import Layout from "../src/components/layout/Layout"
import Posts from "../src/components/Posts"

export default function home() {
  return (
    <Layout>
      <Posts />
    </Layout>
  )
}

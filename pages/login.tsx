import { Button, TextField } from "@mui/material"
import React from "react"
import { useLocalStorage } from "@mantine/hooks"
import { useRouter } from "next/router"
import useLemmyClient from "../src/hooks/useLemmyClient"
import Layout from "../src/components/layout/Layout"

export default function Login() {
  const lemmyClient = useLemmyClient()
  const router = useRouter()

  // state
  const [username, setUsername] = React.useState("")
  const [password, setPassword] = React.useState("")
  const [lemmyJwt, setLemmyJwt] = useLocalStorage({
    key: "lemmy-jwt",
    defaultValue: "",
  })

  const login = React.useCallback(async () => {
    try {
      const result = await lemmyClient.login({
        password,
        username_or_email: username,
      })
      setLemmyJwt(result.jwt ?? "")
    } catch (error) {
      console.error({ error })
    }
  }, [lemmyClient, password, setLemmyJwt, username])

  React.useEffect(() => {
    if (lemmyJwt) {
      void router.push("/home")
    }
  }, [lemmyJwt, router])

  return (
    <Layout>
      <div>
        <TextField
          label="Username or Email"
          type="text"
          autoComplete="username"
          onChange={(event) => setUsername(event.target.value)}
        />
      </div>
      <div>
        <TextField
          label="Password"
          type="password"
          autoComplete="password"
          onChange={(event) => setPassword(event.target.value)}
        />
      </div>
      <div>
        <Button variant="outlined" onClick={login}>
          Login
        </Button>
      </div>
    </Layout>
  )
}

import * as React from "react"
import Head from "next/head"
import { AppProps } from "next/app"
import { ThemeProvider } from "@mui/material/styles"
import CssBaseline from "@mui/material/CssBaseline"
import { CacheProvider, EmotionCache } from "@emotion/react"
import { ApiProvider } from "@reduxjs/toolkit/dist/query/react"
import createEmotionCache from "../src/createEmotionCache"
import { useTheme } from "../src/theme"
import { lemmyApi } from "../src/rtk-query/lemmyApi"

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache()

export interface MyAppProperties extends AppProps {
  emotionCache?: EmotionCache
}

export default function MyApp({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
}: MyAppProperties) {
  const theme = useTheme()

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <ApiProvider api={lemmyApi}>
          <Component {...pageProps} />
        </ApiProvider>
      </ThemeProvider>
    </CacheProvider>
  )
}

import Container from "@mui/material/Container"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import { Button } from "@mui/material"
import { useRouter } from "next/router"
import Link from "../src/Link"

export default function Home() {
  const router = useRouter()

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          my: 4,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Typography variant="h4" component="h1" gutterBottom>
          Calemmty UI - Experimental Lemmy UI
        </Typography>
        <Typography>
          This ui experiment reverse proxies{" "}
          <Link href="https://lemmy.blahaj.zone">lemmy.blahaj.zone</Link>,
          assume that I&apos;m running a man in the middle attack on you!
        </Typography>
        <Button
          variant="outlined"
          onClick={() => {
            void router.push("/login")
          }}
        >
          Login
        </Button>
      </Box>
    </Container>
  )
}

import { useLocalStorage } from "@mantine/hooks"

export default function useLemmyJwt() {
  const lemmyJwtLocalStorage = useLocalStorage({
    key: "lemmy-jwt",
    defaultValue: "",
  })

  return lemmyJwtLocalStorage
}

import { LemmyHttp } from "lemmy-js-client"
import React from "react"

export default function useLemmyClient() {
  const lemmyClient = React.useMemo(() => {
    if (typeof document === "undefined") {
      return new LemmyHttp("")
    }

    const host = document.baseURI

    return new LemmyHttp(host.split("/")[0])
  }, [])

  return lemmyClient
}

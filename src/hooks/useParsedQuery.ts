import { ListingType, SortType } from "lemmy-js-client"
import { useRouter } from "next/router"

export default function useParsedQuery() {
  const router = useRouter()
  const { view, sort } = router.query

  return { view: parseView(view), sort: parseSort(sort) }
}

function parseView(
  view: string | string[] | undefined,
): ListingType | undefined {
  if (!view || view === "") {
    return undefined
  }

  if (Array.isArray(view)) {
    return parseView(view[0])
  }

  if (view === "Subscribed" || view === "Local" || view === "All") {
    return view
  }

  return undefined
}

function parseSort(sort: string | string[] | undefined): SortType | undefined {
  if (!sort || sort === "") {
    return undefined
  }

  if (Array.isArray(sort)) {
    return parseSort(sort[0])
  }

  if (includes(sortOptions, sort)) {
    return sort
  }

  return undefined
}

export const sortOptions = [
  "Active",
  "Hot",
  "New",
  "Old",
  "TopDay",
  "TopWeek",
  "TopMonth",
  "TopYear",
  "TopAll",
  "MostComments",
  "NewComments",
] as const

function includes<T extends U, U>(
  coll: ReadonlyArray<T>,
  element: U,
): element is T {
  return coll.includes(element as T)
}

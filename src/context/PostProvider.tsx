import { PostView } from "lemmy-js-client"
import React, { useContext } from "react"

interface PostContextState {
  postView: PostView
  setPostView: (postView: PostView) => void
}

// eslint-disable-next-line unicorn/no-null
const PostContext = React.createContext<PostContextState | null>(null)

interface PostContextProperties {
  postView: PostView
  children: React.ReactNode
}

export default function PostProvider({
  children,
  postView,
}: PostContextProperties) {
  const [localPostView, setLocalPostView] = React.useState(postView)
  const setPostView = React.useCallback(
    (post: PostView) => setLocalPostView(post),
    [],
  )

  const memoPostView = React.useMemo(
    () => ({ postView: localPostView, setPostView }),
    [localPostView, setPostView],
  )

  return (
    <PostContext.Provider value={memoPostView}>{children}</PostContext.Provider>
  )
}

// eslint-disable-next-line func-style
export function usePostView() {
  const postViewContext = React.useContext(PostContext)

  if (!postViewContext) {
    throw new Error("PostViewContext is null!")
  }

  return postViewContext
}

import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import {
  CreatePostLike,
  GetPosts,
  GetPostsResponse,
  GetSite,
  GetSiteResponse,
  Login,
  LoginResponse,
  PostResponse,
} from "lemmy-js-client"

export const lemmyApi = createApi({
  reducerPath: "lemmyApi",
  baseQuery: fetchBaseQuery({ baseUrl: "/api/v3" }),
  endpoints: (builder) => ({
    login: builder.query<LoginResponse, Login>({
      query: (login) => ({
        url: "user/login",
        method: "post",
        body: login,
      }),
    }),
    posts: builder.query<GetPostsResponse, GetPosts>({
      query: (body) => buildGetUrl("post/list", body),
    }),
    site: builder.query<GetSiteResponse, GetSite>({
      query: (body) => buildGetUrl("site", body),
    }),
    postVote: builder.mutation<PostResponse, CreatePostLike>({
      query: (body) => ({
        url: "post/like",
        method: "post",
        body,
      }),
    }),
  }),
})

// Mutations
export const { usePostVoteMutation } = lemmyApi

// Lazy Queries
export const { useLazyLoginQuery, useLazyPostsQuery, useLazySiteQuery } =
  lemmyApi

// Non-lazy Queries
export const { useLoginQuery, usePostsQuery, useSiteQuery } = lemmyApi

function buildGetUrl<GetBody extends object>(
  baseUrl: string,
  getParametersObejct: GetBody,
) {
  return `${baseUrl}?${encodeGetParameters(getParametersObejct)}`
}

function encodeGetParameters<GetBody extends object>(getBody: GetBody) {
  return new URLSearchParams(Object.entries(getBody)).toString()
}

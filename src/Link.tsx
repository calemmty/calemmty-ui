import * as React from "react"
import clsx from "clsx"
import { useRouter } from "next/router"
import NextLink, { LinkProps as NextLinkProperties } from "next/link"
import MuiLink, { LinkProps as MuiLinkProperties } from "@mui/material/Link"
import { styled } from "@mui/material/styles"

// Add support for the sx prop for consistency with the other branches.
const Anchor = styled("a")({})

interface NextLinkComposedProperties
  extends Omit<React.AnchorHTMLAttributes<HTMLAnchorElement>, "href">,
    Omit<
      NextLinkProperties,
      "href" | "as" | "passHref" | "onMouseEnter" | "onClick" | "onTouchStart"
    > {
  to: NextLinkProperties["href"]
  linkAs?: NextLinkProperties["as"]
}

export const NextLinkComposed = React.forwardRef<
  HTMLAnchorElement,
  NextLinkComposedProperties
>(function NextLinkComposed(properties, reference) {
  const {
    to,
    linkAs,
    replace,
    scroll,
    shallow,
    prefetch,
    legacyBehavior = true,
    locale,
    ...other
  } = properties

  return (
    <NextLink
      href={to}
      prefetch={prefetch}
      as={linkAs}
      replace={replace}
      scroll={scroll}
      shallow={shallow}
      passHref
      locale={locale}
      legacyBehavior={legacyBehavior}
    >
      <Anchor ref={reference} {...other} />
    </NextLink>
  )
})

export type LinkProperties = {
  activeClassName?: string
  as?: NextLinkProperties["as"]
  href: NextLinkProperties["href"]
  linkAs?: NextLinkProperties["as"] // Useful when the as prop is shallow by styled().
  noLinkStyle?: boolean
} & Omit<NextLinkComposedProperties, "to" | "linkAs" | "href"> &
  Omit<MuiLinkProperties, "href">

// A styled version of the Next.js Link component:
// https://nextjs.org/docs/api-reference/next/link
const Link = React.forwardRef<HTMLAnchorElement, LinkProperties>(function Link(
  {
    activeClassName = "active",
    as,
    className: classNameProperties,
    href,
    legacyBehavior,
    linkAs: linkAsProperty,
    locale,
    noLinkStyle,
    prefetch,
    replace,
    role, // Link don't have roles.
    scroll,
    shallow,
    ...other
  },
  reference,
) {
  const router = useRouter()
  const pathname = typeof href === "string" ? href : href.pathname
  const className = clsx(classNameProperties, {
    [activeClassName]: router.pathname === pathname && activeClassName,
  })

  const isExternal =
    typeof href === "string" &&
    (href.indexOf("http") === 0 || href.indexOf("mailto:") === 0)

  if (isExternal) {
    if (noLinkStyle) {
      return (
        <Anchor className={className} href={href} ref={reference} {...other} />
      )
    }

    return (
      <MuiLink className={className} href={href} ref={reference} {...other} />
    )
  }

  const linkAs = linkAsProperty || as
  const nextjsProperties = {
    to: href,
    linkAs,
    replace,
    scroll,
    shallow,
    prefetch,
    legacyBehavior,
    locale,
  }

  if (noLinkStyle) {
    return (
      <NextLinkComposed
        className={className}
        ref={reference}
        {...nextjsProperties}
        {...other}
      />
    )
  }

  return (
    <MuiLink
      component={NextLinkComposed}
      className={className}
      ref={reference}
      {...nextjsProperties}
      {...other}
    />
  )
})

export default Link

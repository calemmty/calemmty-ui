import {
  AppBar,
  Avatar,
  Box,
  Menu,
  MenuItem,
  Skeleton,
  Toolbar,
  Typography,
} from "@mui/material"
import React from "react"
import Link from "../Link"
import useLemmyJwt from "../hooks/useLemmyJwt"
import { useLazySiteQuery } from "../rtk-query/lemmyApi"

export default function TopBar() {
  const [lemmyJwt] = useLemmyJwt()
  const [siteQuery, { isLoading, data }] = useLazySiteQuery()

  React.useEffect(() => {
    if (!lemmyJwt) {
      return
    }

    void siteQuery({ auth: lemmyJwt })
  }, [lemmyJwt, siteQuery])

  if (isLoading || !data) {
    return (
      <AppBar position="static">
        <Skeleton variant="text" sx={{ fontSize: "1rem" }} />
      </AppBar>
    )
  }

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Avatar src={data.site_view.site.icon} />

        <Link href="/home">
          <Typography textAlign="center">{data.site_view.site.name}</Typography>
        </Link>

        {/* <Button variant="outlined">Communites</Button> */}
      </Toolbar>
    </AppBar>
  )
}

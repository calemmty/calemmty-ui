import { Avatar, Grid, Typography } from "@mui/material"
import { Person as LemmyPerson } from "lemmy-js-client"
import Link from "../Link"

interface PersonProperties {
  person: LemmyPerson
}

export default function Person({ person }: PersonProperties) {
  return (
    <Grid container spacing={1}>
      <Grid item>
        <Avatar sx={{ width: 24, height: 24 }} src={person.avatar} />
      </Grid>
      <Grid item>
        <Name person={person} />
      </Grid>
    </Grid>
  )
}

function Name({ person }: PersonProperties) {
  const name = person.display_name ?? person.name
  if (person.local) {
    return <Typography>{name}</Typography>
  }

  const remoteUrl = new URL(person.actor_id).hostname
  return (
    <Link href={person.actor_id}>
      <Typography>{`${name}@${remoteUrl}`}</Typography>
    </Link>
  )
}

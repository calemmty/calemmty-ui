import {
  Button,
  ButtonGroup,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
} from "@mui/material"
import { ListingType, SortType } from "lemmy-js-client"
import { useRouter } from "next/router"
import React from "react"
import useParsedQuery, { sortOptions } from "../hooks/useParsedQuery"

export default function PostsFilter() {
  const router = useRouter()
  const { view, sort } = useParsedQuery()

  const setView = React.useCallback(
    (newView: ListingType) => {
      const searchParameters = new URLSearchParams({
        ...router.query,
        view: newView,
      })

      void router.push(`${router.route}?${searchParameters.toString()}`)
    },
    [router],
  )

  const setSort = React.useCallback(
    (newSort: SortType) => {
      const searchParameters = new URLSearchParams({
        ...router.query,
        sort: newSort,
      })

      void router.push(`${router.route}?${searchParameters.toString()}`)
    },
    [router],
  )

  // Intial query setup
  React.useEffect(() => {
    if (view && sort) {
      return
    }

    const searchParameters = new URLSearchParams(router.query)

    if (!view) {
      searchParameters.append("view", "Subscribed")
    }

    if (!sort) {
      searchParameters.append("sort", "Active")
    }
    void router.push(`${router.route}?${searchParameters.toString()}`)
  }, [router, setView, sort, view])

  return (
    <Stack spacing={2} direction="row">
      <ButtonGroup>
        <Button
          onClick={() => setView("Subscribed")}
          variant={view === "Subscribed" ? "contained" : "outlined"}
        >
          Subscribed
        </Button>
        <Button
          onClick={() => setView("Local")}
          variant={view === "Local" ? "contained" : "outlined"}
        >
          Local
        </Button>
        <Button
          onClick={() => setView("All")}
          variant={view === "All" ? "contained" : "outlined"}
        >
          All
        </Button>
      </ButtonGroup>
      <FormControl sx={{ minWidth: 120 }} size="small">
        <InputLabel id="post-sort">Sort</InputLabel>
        <Select
          id="post-sort"
          labelId="post-sort"
          label="Sort"
          value={sort}
          onChange={(event) => setSort(event.target.value as SortType)}
        >
          {sortOptions.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Stack>
  )
}

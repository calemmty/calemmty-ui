import { Button, Paper, Typography } from "@mui/material"

export default function MenuBar() {
  return (
    <Paper color="secondary">
      <Button sx={{ flexGrow: 1 }}>Feed</Button>
      <Button sx={{ flexGrow: 1 }}>Communites</Button>
    </Paper>
  )
}

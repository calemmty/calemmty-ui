import { Box, Container, Grid } from "@mui/material"
import React from "react"
import TopBar from "../TopBar"
import MenuBar from "./MenuBar"

interface LayoutProperties {
  children: React.ReactNode
}

export default function Layout({ children }: LayoutProperties) {
  return (
    <>
      <TopBar />
      <Grid spacing={1} container>
        <Grid item xs={3}>
          <MenuBar />
        </Grid>
        <Grid item xs={9}>
          <Container>
            <Box>{children}</Box>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

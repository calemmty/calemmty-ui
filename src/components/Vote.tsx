import { Box, IconButton, Typography } from "@mui/material"
import { ArrowUpward } from "@mui/icons-material"
import React from "react"
import { usePostView } from "../context/PostProvider"
import { usePostVoteMutation } from "../rtk-query/lemmyApi"
import useLemmyJwt from "../hooks/useLemmyJwt"

export default function Vote() {
  const [lemmyJwt] = useLemmyJwt()
  const { postView, setPostView } = usePostView()
  const [postVoteMutation, { data, isSuccess, isLoading }] =
    usePostVoteMutation()

  const voteAction = React.useCallback(() => {
    void postVoteMutation({
      auth: lemmyJwt,
      post_id: postView.post.id,
      score: postView.my_vote ? 0 : 1,
    })
  }, [lemmyJwt, postView.my_vote, postView.post.id, postVoteMutation])

  React.useEffect(() => {
    if (isSuccess && data) {
      setPostView(data.post_view)
    }
  }, [data, isSuccess, setPostView])

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <IconButton
        onClick={voteAction}
        disabled={isLoading}
        color={postView.my_vote ? "primary" : "default"}
      >
        <ArrowUpward />
      </IconButton>

      <Typography>{postView.counts.score}</Typography>
    </Box>
  )
}

import { ArrowUpward } from "@mui/icons-material"
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Paper,
  Typography,
} from "@mui/material"
import dayjs from "dayjs"
import { PostView } from "lemmy-js-client"
import relativeTime from "dayjs/plugin/relativeTime"
import Person from "./Person"
import PostProvider from "../context/PostProvider"
import Vote from "./Vote"

dayjs.extend(relativeTime)

interface PostCardProperties {
  postView: PostView
}

export default function PostCard({ postView }: PostCardProperties) {
  return (
    <PostProvider postView={postView}>
      <Paper sx={{ flexGrow: 1 }} elevation={3}>
        <Card sx={{ display: "flex", flexGrow: 1 }}>
          <Vote />
          <Box sx={{ flexGrow: 1, paddingRight: 2 }}>
            <CardHeader
              sx={{ flex: "1 0 auto" }}
              title={<Typography variant="h6">{postView.post.name}</Typography>}
              subheader={
                <Grid container spacing={0.5}>
                  <Grid item>
                    <Person person={postView.creator} />
                  </Grid>
                  <Grid item>
                    <Typography>
                      posted {dayjs(postView.post.published).fromNow(true)} ago
                    </Typography>
                  </Grid>
                </Grid>
              }
              avatar={
                <>
                  <Avatar
                    sx={{ width: 72, height: 72 }}
                    src={postView.community.icon}
                    alt={postView.community.name}
                  />
                  <Typography>{postView.community.name}</Typography>
                </>
              }
            />
            {postView.post.url ? (
              <Paper elevation={0} sx={{ flexGrow: 1 }}>
                <CardMedia
                  sx={{ maxHeight: 380, objectFit: "contain" }}
                  component="img"
                  image={postView.post.url}
                />
              </Paper>
            ) : undefined}
            <CardContent>
              <Typography variant="caption">
                {postView.counts.comments} Comments
              </Typography>
            </CardContent>
          </Box>
        </Card>
      </Paper>
    </PostProvider>
  )
}

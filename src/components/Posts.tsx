import React from "react"
import { Container, List, ListItem, Stack, Typography } from "@mui/material"
import { useLazyPostsQuery } from "../rtk-query/lemmyApi"
import useLemmyJwt from "../hooks/useLemmyJwt"
import PostCard from "./PostCard"
import PostsFilter from "./PostsFilter"
import useParsedQuery from "../hooks/useParsedQuery"

export default function Posts() {
  const [lemmyJwt] = useLemmyJwt()
  const [postsQuery, { isLoading, data }] = useLazyPostsQuery()
  const { view, sort } = useParsedQuery()

  React.useEffect(() => {
    if (!lemmyJwt || !view) {
      return
    }

    void postsQuery({ auth: lemmyJwt, type_: view, sort, limit: 25 })
  }, [lemmyJwt, postsQuery, sort, view])

  if (isLoading) {
    return (
      <>
        <PostsFilter />
        <Typography>Loading...</Typography>
      </>
    )
  }

  if (!data) {
    return (
      <>
        <PostsFilter />
        <Typography>No Posts!</Typography>
      </>
    )
  }

  const { posts } = data

  return (
    <Container sx={{ paddingTop: 2, width: "100%", maxWidth: 900 }}>
      <Stack>
        <PostsFilter />
        <List sx={{ bgcolor: "background.paper" }}>
          {posts.map((post) => (
            <ListItem key={post.post.id} alignItems="flex-start">
              <PostCard postView={post} />
            </ListItem>
          ))}
        </List>
      </Stack>
    </Container>
  )
}

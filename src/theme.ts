import { Roboto } from "next/font/google"
import { createTheme } from "@mui/material/styles"
import { red } from "@mui/material/colors"
import React from "react"
import { useMediaQuery } from "@mui/material"

export const roboto = Roboto({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
  display: "swap",
  fallback: ["Helvetica", "Arial", "sans-serif"],
})

export function useTheme() {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)")

  const themed = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? "dark" : "light",
          primary: {
            main: "#556cd6",
          },
          secondary: {
            main: "#19857b",
          },
          error: {
            main: red.A400,
          },
        },
        typography: {
          fontFamily: roboto.style.fontFamily,
        },
      }),
    [prefersDarkMode],
  )

  return themed
}

/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return {
      fallback: [
        { source: "/api/:path*", destination: "https://lemmy.blahaj.zone/api/:path*" },
      ],
    }
  },
}
